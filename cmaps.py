#!/usr/bin/python3

import os, sys, argparse
import string
import numpy as np
from scipy.sparse import coo_matrix
from matplotlib import pyplot as plt
import networkx as nx
from networkx.algorithms.centrality import edge_betweenness_centrality

def read_contact_map(filename, verbose=False):
	rows = []
	cols = []
	low = []
	up = []
	confs = []
	started = False
	ended = False
	with open(filename, "r") as f:
		l = f.readline()
		while not ended and l != None:
			if not started and l[0] not in string.digits:
				if verbose: print("skipping", l)
				l = f.readline()
				continue
			else:
				started = True
			if started:
				if l == "END\n":
					ended = True
				else:
					if verbose: print("reading", l.strip())
					aux = l.split();
					rows.append(int(aux[0]))
					cols.append(int(aux[1]))
					low.append(int(aux[2]))
					up.append(int(aux[3]))
					confs.append(float(aux[4]))
			l = f.readline()
	return np.array(rows), np.array(cols), np.array(low), np.array(up), np.array(confs)

def diameter(filename):
	sel = read_pdb(filename).centroids()
	
	aux = []
	for i in range(len(sel.dbrefs)):
		seqlen = abs(sel.dbrefs[i].seqend - sel.dbrefs[i].seqbegin)
		
		dbrefsel = sel.resnum(list(range(sel.dbrefs[i].seqbegin, sel.dbrefs[i].seqend + 1)))
		g = distance_graph(dbrefsel, transform_fn=lambda x: x if x <=8 else None)
		diam = nx.algorithms.distance_measures.diameter(g)
		dac = nx.algorithms.assortativity.degree_assortativity_coefficient(g)
		clus = approximation.average_clustering(g)

		aux.append((seqlen, diam, dac, clus))
	
	return aux

def plot(filename):
	sel = read_pdb(filename).centroids()	
	dis = rel_distance_matrix(sel, sequence_min_neighborhood=1)
	plt.figure()
	plt.title(filename)
	plt.imshow(dis.todense())
	plt.savefig("{}.png".format(filename))
	plt.close()
	return dis

def png(filename):
	sel = read_pdb(filename).centroids()
	dis = rel_distance_matrix(sel, sequence_min_neighborhood=1)
	im = Image.fromarray(dis)
	im.save(filename[:-4] + ".jpeg")
	return None

def contrast_normalization(array):
	rank = array.tolist()
	rank.sort(reverse=False)
	for i in range(len(array)):
		array[i] = rank.index(array[i])

def threshold(array, p):
	cum = np.zeros(len(array))	
	for i in range(1, len(array)):
		cum[i] = cum[i - 1] + array[i]
	
	#0-1 reescale	
	#print("cum", cum)
	m = np.max(cum)
	#print("m", m)
	cum /= m
	#print("cum", cum)
	#plt.show()

	a = 0
	for i in range(len(cum)):
		if cum[i] > p:
			break
		else:
			a += 1

	return array[a]

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Select contacts for geometric search of protein fold')
	parser.add_argument("-p", "--plot")
	parser.add_argument("-t", type=float, default=0.9, help="treeshold")	
	parser.add_argument("-c", "--conf", help="weight contact by conf usage (contrast, normal)")
	parser.add_argument("-eb", "--edge-betweenness-centrality", action="store_true")
	parser.add_argument("--top")
	parser.add_argument("input", metavar="INPUT")
	
	args = parser.parse_args()

	rows, cols, low, up, confs = read_contact_map(args.input)
	
	# contrastive normalization
	if args.conf != None:
		if args.conf == "contrast":
			contrast_normalization(confs)
		elif args.conf != "normal":
			print("unknowd method \"", args.conf, "\"for conf handling")
	else:
		for i in range(len(confs)):
			confs[i] = 1		
	
	if args.t:
		t = threshold(confs, args.t)
		print("threshold: ", t)
		idx = confs.tolist().index(t)
		rows = rows[:idx]
		cols = cols[:idx]
		confs = confs[:idx]

	# graph
	graph = nx.Graph()	
	graph.add_weighted_edges_from(zip(rows, cols, confs))
	#nx.draw(graph, with_labels=True, font_weight='bold')
	#plt.show()

	# distance matrix
	N = max(np.max(rows), np.max(cols)) + 1
	distance_matrix = coo_matrix((confs, (rows, cols)), shape=(N, N))
	complete_distance_matrix = distance_matrix + distance_matrix.transpose()
	
	# edge betweeness centrality
	if args.edge_betweenness_centrality:
		edges = edge_betweenness_centrality(graph, normalized=True, weight="weight")
		edges_sorteds = list(edges.items())
		edges_sorteds.sort(key=lambda x: x[1], reverse=True)
		frows = []
		fcols = []
		fvalues = []
		for edge in edges_sorteds:
			frows.append(edge[0][0])
			fcols.append(edge[0][1])
			fvalues.append(edge[1])
		frows = np.array(frows)
		fcols = np.array(fcols)
		fvalues = np.array(fvalues)

		#contrast_normalization(fvalues)	
		k = np.max(fvalues)
		fvalues = (k - fvalues) / k		
		
		for i in range(len(frows)):
			print("{:d} {:d} 0 8 {:.3f}".format(frows[i], fcols[i], fvalues[i]))
		
		complete_distance_matrix = coo_matrix((fvalues, (frows, fcols)), shape=(N, N))

	# plot contacts
	if args.plot:
		plt.imshow(complete_distance_matrix.todense())
		plt.title(args.input)
		plt.savefig(args.plot)
		