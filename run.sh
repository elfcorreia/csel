#!/bin/bash

for f in $1/*.txt; do 
	./cmaps.py $f -p ${f}_confs_ignored.png
	./cmaps.py $f -eb -p ${f}_confs_ignored_filtered.png > ${f}_confs_ignored_filtered.map
	
	./cmaps.py $f -c normal -p ${f}_confs.png
	./cmaps.py $f -eb -c normal -p ${f}_confs_filtered.png > ${f}_confs_filtered.map
	
	./cmaps.py $f -c contrast -p ${f}_confs_normalized.png
	./cmaps.py $f -eb -c contrast -p ${f}_confs_normalized_filtered.png > ${f}_confs_normalized_filtered.map
done